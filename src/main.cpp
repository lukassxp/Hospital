#include "pessoa.hpp"
#include "medico.hpp"
#include "paciente.hpp"
#include "visitante.hpp"

#include <iostream>
#include <string>

using namespace std;

int main (void){
	Paciente *p2, *p = new Paciente("Joao", 25, "Masculino", NULL, 203);
	Medico *m2, *m = new Medico("Dr. Benilson", 65, "Masculino", p, "Neurologista");	
	Visitante *v = new Visitante("Bruna", 18, "Feminino", p, p->getQuarto());

    

	p->setMedico(m);
	
	cout << "*----------------------------------------------------------------------*" << endl;
	cout << "\nPACIENTE: " << p->getNome() << "\n\nIDADE = " << p->getIdade() << "\nSEXO = " << p->getSexo() << "\nMEDICO = ";
	cout << p->medico->getNome() << "\nQUARTO = " << p->getQuarto() << endl;

	cout << "\n*----------------------------------------------------------------------*" << endl;
	cout << "\nMEDICO: " << m->getNome() << "\n\nIDADE = " << m->getIdade() << "\nSEXO = " << m->getSexo() << "\nPACIENTE = ";
	cout << m->paciente->getNome() << "\nESPECIALIDADE = " << m->getEspecialidade() << endl;
	
	cout << "\n*----------------------------------------------------------------------*" << endl;
	cout << "\nVISITANTE: " << v->getNome() << "\n\nIDADE = " << v->getIdade() << "\nSEXO = " << v->getSexo() << "\nVISITADO = ";
	cout << v->paciente->getNome() << "\nQUARTO = " << v->getQuarto() << endl;
	
	m2 = new Medico("Dr. Roberto", 42, "Masculino", p, "Dentista");
	
	p->setMedico(m2);
	
	cout << "\n#########################################################################" << endl;
	cout << "#########################################################################" << endl;
	
	p2 = new Paciente("Josefa", 74, "Feminino", m, 69);
	
	m->setPaciente(p2);
	
	cout << "*----------------------------------------------------------------------*" << endl;
	cout << "\nPACIENTE: " << p->getNome() << "\n\nIDADE = " << p->getIdade() << "\nSEXO = " << p->getSexo() << "\nMEDICO = ";
	cout << p->medico->getNome() << "\nQUARTO = " << p->getQuarto() << endl;

    cout << "\n*----------------------------------------------------------------------*" << endl;
	cout << "\nMEDICO: " << m2->getNome() << "\n\nIDADE = " << m2->getIdade() << "\nSEXO = " << m2->getSexo() << "\nPACIENTE = ";
	cout << m2->paciente->getNome() << "\nESPECIALIDADE = " << m2->getEspecialidade() << endl;
	
	cout << "*----------------------------------------------------------------------*" << endl;
	cout << "\nPACIENTE: " << p2->getNome() << "\n\nIDADE = " << p2->getIdade() << "\nSEXO = " << p2->getSexo() << "\nMEDICO = ";
	cout << p2->medico->getNome() << "\nQUARTO = " << p2->getQuarto() << endl;

    cout << "\n*----------------------------------------------------------------------*" << endl;
	cout << "\nMEDICO: " << m->getNome() << "\n\nIDADE = " << m->getIdade() << "\nSEXO = " << m->getSexo() << "\nPACIENTE = ";
	cout << m->paciente->getNome() << "\nESPECIALIDADE = " << m->getEspecialidade() << endl;
	
	cout << "\n*----------------------------------------------------------------------*" << endl;

	return 0;
}
