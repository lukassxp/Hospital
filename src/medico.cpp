#include "medico.hpp"

Medico::Medico(){

}

Medico::Medico(string nome, int idade, string sexo,class Paciente *paciente, string especialidade){
	this->nome = nome;
	this->idade = idade;
	this->sexo = sexo;
	this->paciente = paciente;
	this->especialidade = especialidade;
}

void Medico::setPaciente(class Paciente *paciente){
	this->paciente = paciente;
}

void Medico::setEspecialidade(string especialidade){
	this->especialidade = especialidade;
}

class Paciente Medico::getPaciente(){
	return *paciente;
}

string Medico::getEspecialidade(){
	return especialidade;
}
